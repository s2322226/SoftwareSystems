package ss.week2;

public class ThreeWayLamp {

    public enum LampSetting {
        OFF,
        LOW,
        MEDIUM,
        HIGH,
        STATE,
        NEXT,
        HELP,
        EXIT
    }

    private static LampSetting currentSetting = LampSetting.OFF;

    public static void switchSetting() {
        switch(currentSetting) {
            case OFF:
                setLampSetting(LampSetting.LOW);
                break;
            case LOW:
                setLampSetting(LampSetting.MEDIUM);
                break;
            case MEDIUM:
                setLampSetting(LampSetting.HIGH);
                break;
            case HIGH:
                setLampSetting(LampSetting.OFF);
                break;
        }
    }

    public static LampSetting getSetting() {
        return currentSetting;
    }

    public static void setLampSetting(LampSetting setting) {
        currentSetting = setting;
    }

}
