package ss.week2;

public class DollarsAndCentsCounter {

    private int dollars = 0;
    private int cents = 0;

    /**
     * @ensures \result >= 0
     * @return dollars count
     */
    public int dollars() {
        if (dollars < 0) {
            dollars = 0;
        }
        return dollars;
    }

    /**
     * @ensures 100 > \result && \result => 0
     * @return cent count
     */
    public int cents() {
        if (cents < 0) {
            cents = 0;
        }
        return cents;
    }

    public void add(int dollars, int cents) {
        this.cents += cents;
        if (this.cents >= 100) {
            this.dollars += this.cents / 100;
            this.cents = this.cents % 100;
        }
        this.dollars += dollars;
    }

    /**
     * @ensures dollars == 0 && cents == 0
     */
    public void reset() {
        dollars = 0;
        cents = 0;
    }

}
