package ss.week2;

import ss.utils.TextIO;

public class ThreeWayLampTUI {

    public static void main(String[] args) {
        ThreeWayLamp lamp = new ThreeWayLamp();

        boolean exit = false;

        printMenu();

        while(!exit) {
            String input = TextIO.getln();

            switch (input) {
                case "OFF":
                    lamp.setLampSetting(ThreeWayLamp.LampSetting.OFF);
                    printLine("Current state: " + lamp.getSetting().name());
                    break;
                case "LOW":
                    lamp.setLampSetting(ThreeWayLamp.LampSetting.LOW);
                    printLine("Current state: " + lamp.getSetting().name());
                    break;
                case "MEDIUM":
                    lamp.setLampSetting(ThreeWayLamp.LampSetting.MEDIUM);
                    printLine("Current state: " + lamp.getSetting().name());
                    break;
                case "HIGH":
                    lamp.setLampSetting(ThreeWayLamp.LampSetting.HIGH);
                    printLine("Current state: " + lamp.getSetting().name());
                    break;
                case "STATE":
                    printLine("Current state: " + lamp.getSetting().name());
                    break;
                case "NEXT":
                    lamp.switchSetting();
                    printLine("Current state: " + lamp.getSetting().name());
                    break;
                case "HELP":
                    printMenu();
                    break;
                case "EXIT":
                    exit = true;
                    break;
            }
        }
    }

    public static void printMenu() {
        printLine("Use one of the following commands: ");
        for (ThreeWayLamp.LampSetting setting : ThreeWayLamp.LampSetting.values()) {
            printLine(setting.name());
        }
    }

    public static void printLine(String line) {
        System.out.println(line);
    }
}
