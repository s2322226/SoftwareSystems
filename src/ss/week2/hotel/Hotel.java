package ss.week2.hotel;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    public static String name;
    static List<Guest> guests;
    static List<Room> rooms;

    public Hotel(String name) {
        this.name = name;
        guests  = new ArrayList<>();
        rooms = new ArrayList<>();
        rooms.add(new Room(1));
        rooms.add(new Room(2));
    }

    public static Room checkIn(String name) {
        // check in guest, return null if already guest with that name
        Room room = getFreeRoom();
        if (room != null) {
            Guest guest = new Guest(name);

            for (Guest curGuest : guests) {
                if (curGuest.equals(guest)) {
                    return null;
                }
            }

            guests.add(guest);
            guest.checkin(room);
            room.setGuest(guest);
            System.out.println("added " + guest.getName() + " in room " + room.getNumber());
            return room;
        }
        return null;
    }

    public static void checkOut(String name) {
        // check out guest
        for (Room room : rooms) {
            if (room.getGuest() != null && room.getGuest().getName().equals(name)) {
                room.getGuest().checkout();
                room.getSafe().deactivate();
                guests.remove(room.getGuest());
                room.setGuest(null);
            }
        }
    }

    public static Room getFreeRoom() {
        // get free room, if no free rooms return null
        for (Room room : rooms) {
            if (room.getGuest() == null) {
                System.out.println("Room " + room.getNumber() + " is free");
                return room;
            }
        }
        System.out.println("No free rooms!");
        return null;
    }

    public static Room getRoom(String name) {
        // get guests room, or return null if not checked in
        for (Room room : rooms) {
            if (room.getGuest() != null && room.getGuest().getName().equals(name)) {
                return room;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        // description of all rooms in the hotel, including the name of the guest and the status of the safe in that room
        return "Hotel " + name + " with rooms " + rooms.toString() + " and guests " + guests.toString();
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public static String getName() {
        return name;
    }
}
