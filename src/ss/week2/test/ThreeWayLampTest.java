package ss.week2.test;

import ss.week2.ThreeWayLamp;

public class ThreeWayLampTest {
    private static ThreeWayLamp lamp;

    public static void main(String[] args) {
        ThreeWayLampTest test = new ThreeWayLampTest();
        test.runTest();
    }

    void runTest() {
        setUp();
        isLampOff();
        checkSequence();
    }

    public static void setUp() {
        lamp = new ThreeWayLamp();
    }

    void isLampOff() {
        System.out.println("Now checking if lamp is turned OFF:");
        if (lamp.getSetting() == ThreeWayLamp.LampSetting.OFF) {
            System.out.println("    Lamp is indeed turned OFF");
        } else {
            System.out.println("    Lamp is still turned ON");
        }
    }

    void checkSequence() {
        System.out.println("Now checking if switchSetting() works:");
        lamp.setLampSetting(ThreeWayLamp.LampSetting.OFF);

        lamp.switchSetting();
        if (lamp.getSetting() == ThreeWayLamp.LampSetting.LOW) {
            System.out.println("    Lamp is indeed LOW");
        } else {
            System.out.println("    Lamp is not LOW, but lamp is " + lamp.getSetting());
        }

        lamp.switchSetting();
        if (lamp.getSetting() == ThreeWayLamp.LampSetting.MEDIUM) {
            System.out.println("    Lamp is indeed MEDIUM");
        } else {
            System.out.println("    Lamp is not MEDIUM, but lamp is " + lamp.getSetting());
        }

        lamp.switchSetting();
        if (lamp.getSetting() == ThreeWayLamp.LampSetting.HIGH) {
            System.out.println("    Lamp is indeed HIGH");
        } else {
            System.out.println("    Lamp is not HIGH, but lamp is " + lamp.getSetting());
        }

        lamp.switchSetting();
        if (lamp.getSetting() == ThreeWayLamp.LampSetting.OFF) {
            System.out.println("    Lamp is indeed OFF");
        } else {
            System.out.println("    Lamp is not OFF, but lamp is " + lamp.getSetting());
        }
    }

}
