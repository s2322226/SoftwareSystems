package ss.week7.chat;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatClientHandler implements Runnable {
    Socket socket;
    TheBestChatServer server;
    PrintWriter writer;
    BufferedReader reader;
    String username;

    public ChatClientHandler(Socket socket, TheBestChatServer server) {
        this.socket = socket;
        this.server = server;
        this.username = null;
        try {
            writer = new PrintWriter(socket.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                String line = reader.readLine();
                if (line != null) {
                    if (username == null) {
                        if (!line.contains("~")) {
                            username = line;
                        }
                    } else {
                        String processedLine = line.replace("SAY~", "");
                        if (!processedLine.contains("~") && line.contains("SAY~")) {
                            server.handleChatMessage(this, processedLine);
                        }
                    }
                } else {
                    close();
                }


            } catch (IOException e) {
                close();
                return;
            }
        }
    }

    public void sendChat(String from, String message) {
        try {
            if (!from.contains("~") && !message.contains("~")) {
                writer.println("FROM~" + from + "~" + message);
                if (writer.checkError()) {
                    close();
                }
            }
        } catch (Exception e) {
            close();
        }
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }
}
