package ss.week7.chat.server;

import java.io.IOException;
import java.net.InetAddress;

public interface ChatClient {

    boolean connect(InetAddress address, int port);

    void close();

    boolean sendUsername(String username);

    boolean sendMessage(String message);

    void addChatListener(ChatListener listener);

    void removeChatListener(ChatListener listener);

}
