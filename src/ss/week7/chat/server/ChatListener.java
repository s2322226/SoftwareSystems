package ss.week7.chat.server;

public interface ChatListener {

    void messageReceived(String from, String message);

}
