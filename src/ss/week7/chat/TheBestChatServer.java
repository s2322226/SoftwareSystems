package ss.week7.chat;

import ss.week7.ClientHandler;
import ss.week7.chat.server.ChatServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class TheBestChatServer implements ChatServer, Runnable {
    public ServerSocket socket;
    Thread serverThread;
    int port;
    List<ChatClientHandler> clients;

    public TheBestChatServer(int port) {
        this.port = port;
        clients = new ArrayList<>();
        start();
    }

    @Override
    public void start() {
        try {
            socket = new ServerSocket(port);
            System.out.println("Started server at port: " + getPort());
            serverThread = new Thread(this);
            serverThread.start();
        } catch (IOException e) {
            System.out.println("Could not start server at port: " + port);
        }
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public void stop() {
        try {
            this.socket.close();
        } catch (IOException e) {
            // ignore
        }
        try {
            this.serverThread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                Socket client = socket.accept();
                System.out.println("accepted");
                ChatClientHandler handler = new ChatClientHandler(client, this);
                addClient(handler);
                new Thread(handler).start();
            } catch (SocketException e) {
                if (socket.isClosed()) {
                    return;
                } else {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void handleChatMessage(ChatClientHandler from, String message) {
        for (ChatClientHandler client : clients) {
            client.sendChat(from.getUsername(), message);
        }
        System.out.println(from.getUsername() + ": " + message);
    }

    public synchronized void addClient(ChatClientHandler client) {
        clients.add(client);
    }

    public synchronized void removeClient(ChatClientHandler client) {
        clients.remove(client);
    }
}
