package ss.week7.chat;

import ss.week7.ClientReceiver;
import ss.week7.chat.server.ChatClient;
import ss.week7.chat.server.ChatListener;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class Client implements ChatClient, Runnable {
    Socket socket = null;
    PrintWriter writer;
    Thread clientThread;
    BufferedReader reader;
    List<ChatListener> listeners;

    public Client() {
        listeners = new ArrayList<>();
    }

    @Override
    public boolean connect(InetAddress address, int port) {
        try {
            socket = new Socket(address, port);
            writer = new PrintWriter(socket.getOutputStream(), true);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.clientThread = new Thread(this);
            this.clientThread.start();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.clientThread.stop();

    }

    @Override
    public boolean sendUsername(String username) {
        try {
            writer.println(username);
        } catch (Exception e) {
            close();
            return false;
        }
        return true;
    }

    @Override
    public boolean sendMessage(String message) {
        try {
            writer.println("SAY~" + message);
        } catch (Exception e) {
            close();
            return false;
        }
        return true;
    }

    @Override
    public void addChatListener(ChatListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeChatListener(ChatListener listener) {
        listeners.remove(listener);
    }

    public void run() {
        while (true) {
            try {
                String line = reader.readLine();
                if (line != null) {
                    String[] parts = line.split("~");
                    if (parts[0].equals("FROM")) {
                        String name = parts[1];
                        String message = parts[2];
                        for (ChatListener listener : listeners) {
                            listener.messageReceived(name, message);
                        }
                    }
                }
            } catch (IOException e) {
                close();
                return;
            }
        }

    }
}
