package ss.week7.chat;

import ss.week7.chat.server.ChatListener;

public class Listener implements ChatListener {

    @Override
    public void messageReceived(String from, String message) {
        System.out.println(from + ": " + message);
    }
}
