package ss.week7.chat;

import ss.utils.TextIO;
import ss.week7.chat.server.ChatClient;
import ss.week7.chat.server.ChatListener;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Chat {
    boolean hasIP = false;
    boolean hasPort = false;
    boolean hasUsername = false;
    String ip;
    int port;
    String username;

    public static void main(String[] args) throws UnknownHostException {
        Chat chat = new Chat();
        chat.run();
    }

    public void run() throws UnknownHostException {

        ChatClient client = new Client();
        client.addChatListener(new Listener());
        Scanner sc = new Scanner(System.in);

        System.out.println("address?");
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (!hasIP) {
                ip = line;
                System.out.println("port?");
                hasIP = true;
            } else if (!hasPort) {
                port = Integer.parseInt(line);
                System.out.println("username?");
                hasPort = true;
                client.connect(InetAddress.getByName(ip), port);
            } else if (!hasUsername) {
                username = line;
                hasUsername = true;
                client.sendUsername(username);
            } else {
                if (line.equals("quit")) {
                    break;
                } else {
                    client.sendMessage(line);
                }
            }
        }
        client.close();
    }
}
