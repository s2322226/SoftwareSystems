package ss.week7;

import java.io.*;
import java.net.Socket;

public class ClientReceiver implements Runnable {
    Socket socket;
    Reader reader;

    public ClientReceiver(Socket socket) {
        this.socket = socket;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                reader.ready();
                System.out.println(new BufferedReader(reader).readLine());
            } catch (IOException e) {
                break;
            }
        }
    }
}
