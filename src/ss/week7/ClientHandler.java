package ss.week7;

import ss.calculator.StreamCalculator;
import ss.week4.calculator.StackBasedCalculator;
import ss.week5.calculator.StackBasedStreamCalculator;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {

    StreamCalculator calculator;
    Socket socket;
    Reader reader;
    PrintWriter writer;

    public ClientHandler(Socket socket) {
        this.socket = socket;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        calculator = new StackBasedStreamCalculator(new StackBasedCalculator());
    }

    @Override
    public void run() {
        System.out.println("Client Connected");
        // writer.println("You have successfully connected to the server, enjoy muppet!");
        while (true) {
            try {
                reader.ready();
                calculator.process(reader, writer);
            } catch (IOException e) {
                break;
            }
        }
    }

}
