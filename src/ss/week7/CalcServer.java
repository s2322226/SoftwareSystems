package ss.week7;

import ss.calculator.CalculatorServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class CalcServer implements CalculatorServer, Runnable {
    public ServerSocket socket;
    Thread serverThread;
    boolean running;

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                Socket client = socket.accept();
                new Thread(new ClientHandler(client)).start();
            } catch (SocketException e) {
                if (socket.isClosed()) {
                    return;
                } else {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void start(int port) throws IOException {
        try {
            this.socket = new ServerSocket(port);
            System.out.println("Started server at port: " + getPort());
            this.running = true;
            this.serverThread = new Thread(this);

            serverThread.start();
        } catch (IOException e) {
            System.out.println("Could not start server at port: " + port);
        }
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public void stop() {
        try {
            this.running = false;
            this.socket.close();
        } catch (IOException e) {
            // ignore
        }
        try {
            this.serverThread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
