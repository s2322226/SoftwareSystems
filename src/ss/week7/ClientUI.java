package ss.week7;

import ss.calculator.CalculatorServer;
import ss.week5.calculator.StackBasedCalculatorFactory;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientUI {
    CalculatorServer server;
    String ip = "";
    int port;
    Socket socket;
    PrintWriter writer;

    public static void main(String[] args) {
        ClientUI ui = new ClientUI();
        ui.run();
    }

    public void run() {
        StackBasedCalculatorFactory factory = new StackBasedCalculatorFactory();
        server = factory.makeCalculatorServer();

        Scanner sc = new Scanner(System.in);
        boolean hasIP = false;
        boolean hasPort = false;

        System.out.println("What IP would you like to connect to?");
        while (sc.hasNextLine()) {
            if (!hasIP) {
                ip = sc.nextLine();
                System.out.println("What port would you like to connect on?");
                hasIP = true;
            } else if (!hasPort) {
                try {
                    port = Integer.parseInt(sc.nextLine());
                    System.out.println("Attempting to connect to " + ip + ":" + port);
                    hasPort = true;
                    if (!connectToServer()) {
                        hasIP = false;
                        hasPort = false;
                        System.out.println("What IP would you like to connect to?");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Port needs to be a number, you muppet!");
                }
            } else {
                String line = sc.nextLine();
                if (line.equals("quit")) {
                    try {
                        socket.close();
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    writer.println(line);
                }
            }
        }
    }

    public boolean connectToServer() {
        try {
            socket = new Socket(ip, port);
            new Thread(new ClientReceiver(socket)).start();
            writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (SocketException e) {
            return false;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
