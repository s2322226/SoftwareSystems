package ss.week7;

import ss.calculator.CalculatorServer;
import ss.utils.TextIO;
import ss.week5.calculator.StackBasedCalculatorFactory;

import java.io.IOException;
import java.util.Scanner;

public class ServerUI {
    
    static CalculatorServer server;
    static boolean serverStarted;

    public static void main(String[] args) {
        System.out.println("What port should the server run on?");

        Scanner sc = new Scanner(System.in);

        while (sc.hasNextLine()) {
            String nextLine = sc.nextLine();
            if (!serverStarted) {
                try {
                    int port = Integer.parseInt(nextLine);
                    StackBasedCalculatorFactory factory = new StackBasedCalculatorFactory();
                    server = factory.makeCalculatorServer();
                    server.start(port);
                    serverStarted = true;
                } catch (NumberFormatException e) {
                    System.out.println("Port needs to be a number, you muppet!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (nextLine.equals("quit")) {
                    server.stop();
                }
            }
        }
    }
}
