package ss.week3.password;

public class BasicChecker implements Checker{

    @Override
    public boolean acceptable(String suggestion) {
        if ( suggestion.length() >= 6 && !suggestion.contains(" ")) {
            return true;
        }
        return false;
    }

    @Override
    public String generatePassword() {
        return "AcceptablePassword123";
    }
}
