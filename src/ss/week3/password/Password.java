package ss.week3.password;

public class Password {
    public static final String INITIAL = "initial123";
    private static String currentPassword;

    public Checker checker;

    public Password(Checker checker) {
        currentPassword = INITIAL;
        this.checker = checker;
    }

    public Password() {
        checker = new BasicChecker();
        new Password(checker);
    }

    /**
     * Tests if a given word is equal to the current password.
     * @param test - Word that should be tested
     * @return true If test is equal to the current password
     */
    public boolean testWord(String test) {
        if ( test.equals(currentPassword)) {
            return true;
        }
        return false;
    }

    /**
     * Changes this password.
     * @param oldpass - The Current password
     * @param newpass - The new password
     * @return true If oldpass is equal to the current password and newpass is an acceptable password
     */
    public boolean setWord(String oldpass, String newpass) {
        if (oldpass.equals(currentPassword) && checker.acceptable(newpass)) {
            currentPassword = newpass;
            return true;
        }
        return false;
    }

    public Checker getChecker() {
        return checker;
    }

    public String getInitPass() {
        return INITIAL;
    }

    public static String getCurrentPassword() {
        return currentPassword;
    }
}
