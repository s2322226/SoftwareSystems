package ss.week3.password;

public class StrongChecker extends BasicChecker{

    @Override
    public boolean acceptable(String suggestion) {
        if ( suggestion.length() >= 6 && !suggestion.contains(" ") && Character.isLetter(suggestion.charAt(0)) && Character.isDigit(suggestion.charAt(suggestion.length()))) {
            return true;
        }
        return false;
    }
}
