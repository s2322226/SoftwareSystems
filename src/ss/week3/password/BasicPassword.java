package ss.week3.password;

public class BasicPassword {

    public static final String INITIAL = "initial";
    private static String currentPassword;

    public BasicPassword() {
        currentPassword = INITIAL;
    }

    /**
     * Checks if suggestions is a viable password.
     * @param suggestion - Potential password that should be verified
     * @return true if password is eligible, false if password is not eligible
     */
    public boolean acceptable(String suggestion) {

        return suggestion.length() >= 6 && !suggestion.contains(" ");
    }

    /**
     * Tests if a given word is equal to the current password.
     * @param test - Word that should be tested
     * @return true If test is equal to the current password
     */
    public boolean testWord(String test) {

        return test.equals(currentPassword);
    }

    /**
     * Changes this password.
     * @param oldpass - The Current password
     * @param newpass - The new password
     * @return true If oldpass is equal to the current password and newpass is an acceptable password
     */
    public boolean setWord(String oldpass, String newpass) {
        if (oldpass.equals(currentPassword) && acceptable(newpass)) {
            currentPassword = newpass;
            return true;
        }
        return false;
    }





}
