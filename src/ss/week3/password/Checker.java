package ss.week3.password;

public interface Checker {

    /**
     * Checks if suggestions is a viable password.
     * @ensures suggestion meets specified conditions
     * @param suggestion - Potential password that should be verified
     * @return true if password is eligible, false if password is not eligible
     */
    public boolean acceptable(String suggestion);

    /**
     * Returns an example of an acceptable password.
     *
     * @return String - Example Password
     */
    public String generatePassword();

}
