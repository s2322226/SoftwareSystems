package ss.week3.bill;

import java.util.ArrayList;
import java.util.List;

/**
 * Bill having multiple items. Lab assignment Module 2
 */
public class Bill {

    double sum;
    public List<Item> items;
    Printer printer;

    /**
     * Abstraction of the items on the bill. Every item has a price and description; the description is available with toString().
     */
    public interface Item {
        /**
         * Returns the price of this Item.
         * @return the price of this Item.
         */
        double getPrice();
    }

    /**
     * Constructs a Bill sending the output to a given Printer.
     * @param printer - the printer to send the bill to
     */
    public Bill(Printer printer) {
        sum = 0;
        items = new ArrayList<>();
        this.printer = printer;
    }

    /**
     * Adds an item to this Bill. The item is sent to the printer, and the price is added to the sum of the Bill.
     * @param item - the new item
     */
    public void addItem(Bill.Item item) {
        items.add(item);
        printer.printLine(item.toString(), item.getPrice());
        sum += item.getPrice();
    }

    /**
     * Sends the sum total of the bill to the printer.
     */
    public void close() {
        printer.printLine("Total", sum);
    }

    /**
     * Returns the current sum total of the Bill.
     * @return the current sum total of the Bill.
     */
    public double getSum() {
        return sum;
    }
}
