package ss.week3.bill;

public class StringPrinter implements Printer {

    String lines = "";


    public static void main(String[] args) {
        StringPrinter printer = new StringPrinter();
        printer.printLine("apple", 2.22);
        printer.printLine("pear", 3.33);
        printer.printLine("banana", 4.44);
        System.out.print(printer.getResult());
    }

    public void printLine(String text, double price) {
        lines = lines + format(text, price);
    }

    public String getResult() {
        return lines;
    }

}
