package ss.week3.bill;

public interface Printer {

    default String format(String text, double price) {
        String newPrice = String.format("%.2f", price);
        String formattedString = String.format("%-15s %15s\n", text, newPrice);

        return formattedString;
    }

    void printLine(String text, double price);
}
