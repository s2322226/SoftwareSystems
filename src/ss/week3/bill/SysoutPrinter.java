package ss.week3.bill;

public class SysoutPrinter implements Printer {

    public static void main(String[] args) {
        SysoutPrinter printer = new SysoutPrinter();
        printer.printLine("apple", 2.22);
        printer.printLine("pear", 3.33);
        printer.printLine("banana", 4.44);
    }

    public void printLine(String text, double price) {
        System.out.print(format(text, price));
    }

}
