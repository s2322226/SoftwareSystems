package ss.week3.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import ss.week3.bill.Bill;
import ss.week3.bill.StringPrinter;

public class BillTest {

    private Bill bill;

    @BeforeEach
    public void setUp() {
        bill = new Bill(new StringPrinter());
    }

    @Test
    public void testBeginState() {
        assertTrue(bill.items.isEmpty());
    }

    @Test
    public void testNewItem() {
        Item item = new Item("banana", 0.50);
        bill.addItem(item);
        assertTrue(bill.items.get(0).toString().contains(item.toString()));
        assertEquals(item.getPrice(), bill.items.get(0).getPrice());
    }

    public class Item implements Bill.Item {
        String text;
        double price;

        public Item(String text, double price) {
            this.text = text;
            this.price = price;
        }

        @Override
        public double getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return text;
        }
    }

}
