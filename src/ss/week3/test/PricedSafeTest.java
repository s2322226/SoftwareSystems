package ss.week3.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ss.week3.bill.Bill;
import ss.week3.hotel.PricedSafe;

public class PricedSafeTest {

    private PricedSafe safe;
    private static final double PRICE = 6.36;
    private static final String PRICE_PATTERN = ".*6[.,]36.*";

    public String CORRECT_PASSWORD;
    public String WRONG_PASSWORD;


    @BeforeEach
    public void setUp() throws Exception {
        safe = new PricedSafe(PRICE);
        CORRECT_PASSWORD = safe.getPassword().getInitPass();
        WRONG_PASSWORD = CORRECT_PASSWORD + "WRONG";
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());
    }

    @Test
    public void testIsBillItem() throws Exception {
        // Checks if Bill.Item is correctly implemented
    	assertTrue(safe instanceof Bill.Item,
    			"safe should be an instance of Bill.Item.");

        // Checks if getPrice method properly works
        assertEquals(PRICE, safe.getPrice(), 0,
        		"GetPrice should return the price of the safe.");
    }

    @Test
    public void testToString() throws Exception {
        // Checks if the toString method correctly displays the price of the safe
        assertEquals("Price of this safe: " + PRICE, safe.toString(), "toString should return the price of the safe in a string");
    }

    @Test
    public void testActivateSafe() throws Exception {
        // Check if safe is deactivated and closed after activating with wrong password
        safe.activate(WRONG_PASSWORD);
        assertFalse(safe.activated, "safe should be deactivated");
        assertFalse(safe.opened, "safe should be closed");

        // Check if safe is activated and closed after activating with correct password
        safe.activate(CORRECT_PASSWORD);
        assertTrue(safe.activated, "safe should be activated");
        assertFalse(safe.opened, "safe should be closed");
    }

    @Test
    public void testOpenSafe() throws Exception {
        // Check if safe is closed and deactivated after opening with wrong password while safe is deactivated
        safe.open(WRONG_PASSWORD);
        assertFalse(safe.opened, "safe should be closed");
        assertFalse(safe.activated, "safe should be deactivated");

        // Check if safe is closed and deactivated after opening with correct password while safe is deactivated
        safe.open(CORRECT_PASSWORD);
        assertFalse(safe.opened, "safe should be closed");
        assertFalse(safe.activated, "safe should be deactivated");

        // Activates and verifies activation of safe
        safe.activate(CORRECT_PASSWORD);
        assertTrue(safe.activated, "safe should be activated");

        // Checks if safe is closed and activated after opening with wrong password while safe is activated
        safe.open(WRONG_PASSWORD);
        assertFalse(safe.opened, "safe should be closed");
        assertTrue(safe.activated, "safe should be activated");

        // Checks if safe is opened and activated after opening with correct password while safe is activated
        safe.open(CORRECT_PASSWORD);
        assertTrue(safe.opened, "safe should be opened");
        assertTrue(safe.activated, "safe should be activated");
    }

    @Test
    public void testCloseSafe() throws Exception {
        safe.activate(CORRECT_PASSWORD);
        safe.open(CORRECT_PASSWORD);

        // Checks if safe is closed and activated after closing while safe is opened and activated
        safe.close();
        assertTrue(safe.activated, "safe should be activated");
        assertFalse(safe.opened, "safe should be closed");

        safe.open(CORRECT_PASSWORD);

        // Checks if safe is closed and deactivated after deactivating while safe is opened and activated
        safe.deactivate();
        assertFalse(safe.activated, "safe should be deactivated");
        assertFalse(safe.opened, "safe should be closed");

        // Checks if safe is closed and deactivated after closing while safe is opened and deactivated
        safe.close();
        assertFalse(safe.activated, "safe should be deactivated");
        assertFalse(safe.opened, "safe should be closed");

    }
}
