package ss.week3.hotel;

public class Safe {

    public boolean opened;
    public boolean activated;

    public Safe() {
        opened = false;
        activated = false;
    }

    public void activate() {
        activated = true;
    }

    public void deactivate() {
        close();
        activated = false;
    }

    public void open() {
        if (activated) {
            opened = true;
        }
    }

    public void close() {
        opened = false;
    }

    public boolean isActive() {
        return activated;
    }

    public boolean isOpen() {
        return opened;
    }
}
