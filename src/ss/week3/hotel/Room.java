package ss.week3.hotel;


public class Room {
    private int number;
    private Guest guest;
    private Safe safe;

    /**
    * Creates a <code>Room</code> with the given number, without a guest.
    * @param number of the new <code>Room</code>
    */
    public Room(int number) {
        this(number, new Safe());
    }

    /**
     * Creates a <code>Room</code> with the given number and safe, without a guest.
     *
     * @param number
     * @param safe
     */
    public Room(int number, Safe safe) {
        this.number = number;
        this.safe = safe;
    }

    /**
     * Returns the number of this Room
     */
    public int getNumber() {
    	return number;
    }

    /**
     * Returns the current guest living in this Room
     * @return the guest of this Room, null if not rented
     */
    public Guest getGuest() {
    	return guest;
    }


    /**
     * Assigns a Guest to this Room.
     * @param guest the new guest renting this Room, if null is given, Room is empty afterwards
     */
    public void setGuest(Guest guest) {
    	this.guest = guest;
    }

    /**
     * Returns string with descriptive information of the room
     *
     * @return string
     */
    @Override
    public String toString() {
        if (guest == null) {
            return "Room " + number + " with no guest ";
        }
        return "Room " + number + " with guest " + guest.getName();
    }

    /**
     * Returns the safe that is allocated to the room
     *
     * @return safe
     */
    public Safe getSafe() {
        return safe;
    }

    /**
     * Sets the value of the safe allocated to the room
     *
     * @param safe
     */
    public void setSafe(Safe safe) {
        this.safe = safe;
    }
}
