package ss.week3.hotel;

import ss.week3.bill.Bill;

public class PricedRoom extends Room implements Bill.Item {

    double price;

    public PricedRoom(int number, double price, double safePrice) {
        super(number, new PricedSafe(safePrice));
        this.price = price;
    }

    @Override
    public String toString() {
        if (getGuest() == null) {
            return "Room " + getNumber() + " with no guest, costs: " + getPrice();
        }
        return "Room " + getNumber() + " with guest" + getGuest().getName() + ", costs: " + getPrice();
    }

    @Override
    public double getPrice() {
        return price;
    }
}
