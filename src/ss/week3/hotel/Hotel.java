package ss.week3.hotel;

import ss.week3.bill.Bill;
import ss.week3.bill.Printer;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    public static String name;
    static List<Guest> guests;
    static List<Room> rooms;
    public static double ROOM_PRICE;
    public static double SAFE_PRICE;

    public Hotel(String name) {
        this.name = name;
        guests  = new ArrayList<>();
        rooms = new ArrayList<>();
        ROOM_PRICE = 1.11;
        SAFE_PRICE = 2.22;
        rooms.add(new PricedRoom(1, ROOM_PRICE, SAFE_PRICE));
        rooms.add(new Room(2));
    }

    public static Room checkIn(String name) {
        // check in guest, return null if already guest with that name
        Room room = getFreeRoom();
        if (room != null) {
            Guest guest = new Guest(name);

            for (Guest curGuest : guests) {
                if (curGuest.equals(guest)) {
                    return null;
                }
            }

            guests.add(guest);
            guest.checkin(room);
            room.setGuest(guest);
            //System.out.println("added " + guest.getName() + " in room " + room.getNumber());
            return room;
        }
        return null;
    }

    public static void checkOut(String name) {
        // check out guest
        for (Room room : rooms) {
            if (room.getGuest() != null && room.getGuest().getName().equals(name)) {
                room.getGuest().checkout();
                room.getSafe().deactivate();
                guests.remove(room.getGuest());
                room.setGuest(null);
            }
        }
    }

    public static Room getFreeRoom() {
        // get free room, if no free rooms return null
        for (Room room : rooms) {
            if (room.getGuest() == null) {
                //System.out.println("Room " + room.getNumber() + " is free");
                return room;
            }
        }
        //System.out.println("No free rooms!");
        return null;
    }

    public static Room getRoom(String name) {
        // get guests room, or return null if not checked in
        for (Room room : rooms) {
            if (room.getGuest() != null && room.getGuest().getName().equals(name)) {
                return room;
            }
        }
        return null;
    }

    public Bill getBill(String guestName, int nights, Printer printer) {
        Bill bill = new Bill(printer);

        for (Guest guest : guests) {
            if (guest.getName().equals(guestName)) {
                if (guest.getRoom() instanceof PricedRoom) {
                    for (int i = 0; i < nights; i++) {
                        bill.addItem((PricedRoom) guest.getRoom());
                    }
                    if (guest.getRoom().getSafe().isActive()) {
                        bill.addItem((PricedSafe) (guest.getRoom()).getSafe());
                    }
                    bill.close();
                    return bill;
                }
            }
        }

        return null;
    }

    public class Item implements Bill.Item {
        String text;
        double price;

        public Item(String text, double price) {
            this.text = text;
            this.price = price;
        }

        @Override
        public double getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    @Override
    public String toString() {
        // description of all rooms in the hotel, including the name of the guest and the status of the safe in that room
        return "Hotel " + name + " with rooms " + rooms.toString() + " and guests " + guests.toString();
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public static String getName() {
        return name;
    }
}
