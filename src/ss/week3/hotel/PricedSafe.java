package ss.week3.hotel;

import ss.week3.bill.Bill;
import ss.week3.password.Password;

public class PricedSafe extends Safe implements Bill.Item {

    private Password password;
    private double price;

    public PricedSafe(double price) {
        this.price = price;
        password = new Password();
    }

    public static void main(String[] args) {
        PricedSafe safe = new PricedSafe(1.11);
        safe.activate("fsfdasdf");
    }

    /**
     * Activates safe if given password matches correct password of safe
     * @param password - provided password
     */
    public void activate(String password) {
        assert password != null : "Password is null";
        if (this.password.getCurrentPassword().equals(password)) {
            activated = true;
        } /*else {
            System.out.println("Password incorrect, please try again.");
        }*/
    }

    /**
     * Sends message about using a password
     */
    @Override
    public void activate() {
        System.out.println("You need to enter a password to open this safe.");
    }

    /**
     * Opens safe if safe is activated and given password matches correct password of safe
     * @param password - provided password
     */
    public void open(String password) {
        //assert this.password.getCurrentPassword().equals(password);
        if (this.password.getCurrentPassword().equals(password) && activated) {
            opened = true;
        } else {
            System.out.println("Password incorrect, please try again.");
        }
    }

    /**
     * Sends message about using a password
     */
    @Override
    public void open() {
        System.out.println("You need to enter a password to open this safe.");
    }


    @Override
    public double getPrice() {
        return price;
    }

    public Password getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Price of this safe: " + price;
    }
}
