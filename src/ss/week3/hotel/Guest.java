package ss.week3.hotel;



/**
 * Guest object
 *
 * @author Sander and Cas
 */
public class Guest {

    private String name;
    private Room room;

    /**
     * Creates a <code>Guest</code> with the given name, without a room.
     * @param name of the new <code>Guest</code>
     */
    public Guest(String name) {
        this.name = name;
    }

    /**
     * Returns the name of the guest
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the room of the guest
     * @return room
     */
    public Room getRoom() {
        return room;
    }

    /**
     * Attempts to check the guest in the specified room
     * @param room for the guest to be checked in
     * @ensures room != null
     * @return result of check in attempt
     */
    public boolean checkin(Room room) {
        if (this.room == null && room.getGuest() == null) {
            this.room = room;
            room.setGuest(this);
            return true;
        }
        return false;
    }

    /**
     * Attempts to check the guest out of their room
     * @return result of check out attempt
     */
    public boolean checkout() {
        if (room != null) {
            room.setGuest(null);
            room = null;
            return true;
        }
        return false;
    }

    /**
     * Returns a string with descriptive information of the guest
     *
     * @return string
     */
    @Override
    public String toString() {
        return "Guest " + name + " in room " + room;
    }
}
