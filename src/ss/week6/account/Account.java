package ss.week6.account;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
	protected double balance = 0.0;
	private Lock lock;
	private Condition lowBalance;

	public Account() {
		lock = new ReentrantLock();
		lowBalance = lock.newCondition();
	}

	public void transaction(double amount) {
		lock.lock();
		if (balance + amount < -1000) {
			System.out.println("Balance - amount = " + (balance + amount));
		}
		try {
			while (balance + amount < -1000) {
				lowBalance.await();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance = balance + amount;
		System.out.println("New Balance = " + balance);
		lowBalance.signal();
		lock.unlock();
	}

	public double getBalance() {
		return balance;

	}
}
