package ss.week6;

import ss.week5.calculator.StackBasedCalculatorFactory;

import java.io.*;
import java.time.Duration;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertTimeout;

public class CalcThreadRunner {

    public static void main(String[] args) {
        try {
            System.out.println("Wanna do some math? -push -pop -dup -add -sub -mult -div -mod -quit");

            // creates factory
            StackBasedCalculatorFactory factory = new StackBasedCalculatorFactory();

            // creates PipedReader and PipedWriter
            var pr1 = new PipedReader();
            var pw1 = new PipedWriter(pr1);

            // creates runnable stream calculator
            var rsc = factory.makeRunnableStreamCalculator(pr1, new PrintWriter(System.out));
            if (rsc == null) return; // oh, not yet implemented

            // creates and starts thread using runnable calculator
            var thread = new Thread(rsc);
            thread.start();

            // creates printwriter and buffferedreader
            var pw = new PrintWriter(pw1, true);

            // creates Scanner
            Scanner sc = new Scanner(System.in);


            // runs calculator process method in a loop while scanner is running
            while (sc.hasNextLine()) {
                String nextLine = sc.nextLine();
                if (nextLine.equals("quit")) {
                    System.out.println("Shutting down calculator");
                    break;
                } else {
                    pw.println(nextLine);
                }

                // uses dup() and pop() to return relevant value to the system out
//                pw.println("value");
            }
            pw.close();

            // wait at most 1 second for the thread to stop; if it doesn't, this will trigger an assertion error
            assertTimeout(Duration.ofSeconds(1), () -> thread.join());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
