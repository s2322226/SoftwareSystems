package ss.week6;

import ss.week4.calculator.StackBasedCalculator;
import ss.week5.calculator.StackBasedStreamCalculator;

import java.io.*;

public class RunnableCalculator implements Runnable {
    Reader reader;
    Writer writer;
    StackBasedStreamCalculator calculator;

    public RunnableCalculator(Reader reader, Writer writer) {
        this.reader = reader;
        this.writer = writer;
        calculator = new StackBasedStreamCalculator(new StackBasedCalculator());
    }

    @Override
    public void run() {
        while (true) {
            try {
                reader.ready();
                calculator.process(reader, writer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
