package ss.week6.threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FinegrainedIntCell implements IntCell {
    private final ThreadVal buffer;
    private final Lock lock;
    final Condition hasValue;
    final Condition noValue;

    public FinegrainedIntCell() {
        lock = new ReentrantLock();
        buffer = new ThreadVal();
        hasValue = lock.newCondition();
        noValue = lock.newCondition();
    }

    @Override
    public void setValue(int val) {
        lock.lock();
        try {
            while (!buffer.isEmpty()) {
                noValue.await();
            }
            buffer.setVal(val);
            buffer.setEmpty(false);
            hasValue.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int getValue() {
        lock.lock();
        try {
            while (buffer.isEmpty()) {
                hasValue.await();
            }
            buffer.setEmpty(true);
            noValue.signal();
            return buffer.getVal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return 0;
    }
}
