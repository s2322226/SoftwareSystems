package ss.week6.threads;

public class TestConsole implements Runnable {

    public static void main(String[] args) {
        new Thread(new TestConsole(), "Thread A").start();
        new Thread(new TestConsole(), "Thread B").start();
    }

    @Override
    public void run() {
        sum("get number ");
    }

    private void sum(String text) {
        String currentThread = Thread.currentThread().getName();
        int number1 = Console.readInt(currentThread + ": " + text + 1 + "?");
        int number2 = Console.readInt(currentThread + ": " + text + 2 + "?");

        System.out.println(currentThread + number1 + " + " + number2 + " = " + (number1 + number2));
    }
}
