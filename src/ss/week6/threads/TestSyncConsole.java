package ss.week6.threads;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestSyncConsole implements Runnable {

    private static Lock lock;

    public static void main(String[] args) {
        lock = new ReentrantLock();
        new Thread(new TestSyncConsole(), "Thread A").start();
        new Thread(new TestSyncConsole(), "Thread B").start();
    }

    @Override
    public void run() {
        sum("get number ");
    }

    private void sum(String text) {
        lock.lock();
        String currentThread = Thread.currentThread().getName();
        int number1 = SyncConsole.readInt(currentThread + ": " + text + 1 + "?");
        int number2 = SyncConsole.readInt(currentThread + ": " + text + 2 + "?");
        SyncConsole.println(currentThread + ": " + number1 + " + " + number2 + " = " + (number1 + number2));
        lock.unlock();
    }
}
