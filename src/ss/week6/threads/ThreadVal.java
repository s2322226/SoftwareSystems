package ss.week6.threads;

public class ThreadVal {
    private int val;
    private boolean empty;

    public ThreadVal() {
        val = 0;
        empty = true;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }
}
