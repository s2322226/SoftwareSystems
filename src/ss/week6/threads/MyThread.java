package ss.week6.threads;

import ss.week6.account.Account;

public class MyThread implements Runnable {
    double theAmount;
    int theFrequency;
    public Account theAccount;

    public MyThread(double amount, int frequency, Account account) {
        this.theAmount = amount;
        this.theFrequency = frequency;
        this.theAccount = account;

    }

    @Override
    public void run() {
        for (int i = 0; i < theFrequency; i++) {
            theAccount.transaction(theAmount);
        }
    }
}
