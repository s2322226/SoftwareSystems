package ss.week6.threads;

import ss.week6.account.Account;

public class AccountSync {

    public static void main(String[] args) {
        Account account = new Account();
        Thread depositThread = new Thread(new MyThread(15.0, 1100, account));
        Thread withdrawalThread = new Thread(new MyThread(-20.0, 1000, account));

        depositThread.start();
        withdrawalThread.start();

        while (depositThread.isAlive() || withdrawalThread.isAlive()) {

        }

        System.out.println(account.getBalance());
    }
}
