package ss.week6.threads;

public class SynchronizedIntCell implements IntCell {
    private final ThreadVal buffer;

    public SynchronizedIntCell() {
        buffer = new ThreadVal();
        buffer.setVal(0);
    }

    @Override
    public void setValue(int val) {
        synchronized (buffer) {
            while (!buffer.isEmpty()) {
                try {
                    buffer.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            buffer.notifyAll();
            buffer.setVal(val);
            buffer.setEmpty(false);
        }
    }

    @Override
    public int getValue() {
        synchronized (buffer) {
            while (buffer.isEmpty()) {
                try {
                    buffer.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            buffer.notifyAll();
            buffer.setEmpty(true);
            return buffer.getVal();
        }
    }
}
