package ss.week6.dictionaryattack;

import org.apache.commons.codec.binary.Hex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;


public class DictionaryAttack {
	private Map<String, String> passwordMap;
	private Map<String, String> hashDictionary;

	private static final String PATH = "./src/ss/week6/test/";

	/**
	 * Reads a password file. Each line of the password file has the form:
	 * username: encodedpassword
	 * 
	 * After calling this method, the passwordMap class variable should be
	 * filled with the content of the file. The key for the map should be
	 * the username, and the password hash should be the content.
	 * @param filename
	 */
	public void readPasswords(String filename) {
		// To implement
		try {
			passwordMap = new HashMap<>();
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split(": ");
				passwordMap.put(parts[0], parts[1]);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Given a password, return the MD5 hash of a password. The resulting
	 * hash (or sometimes called digest) should be hex-encoded in a String.
	 * @param password
	 * @return
	 */
	public String getPasswordHash(String password) {
		// To implement
		try {
			byte[] data = password.getBytes();
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			md.update(data);
			byte[] digest = md.digest();
			return Hex.encodeHexString(digest);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Checks the password for the user from the password list. If the user
	 * does not exist, returns false.
	 * @param user
	 * @param password
	 * @return whether the password for that user was correct.
	 */
	public boolean checkPassword(String user, String password) {
         // To implement
		String hashedPassword = getPasswordHash(password);
		if (passwordMap.containsKey(user) && hashedPassword.equals(passwordMap.get(user))) {
			return true;
		}
		return false;
	}

	/**
	 * Reads a dictionary from file (one line per word) and use it to add
	 * entries to a dictionary that maps password hashes (hex-encoded) to
     * the original password.
	 * @param filename filename of the dictionary.
	 */
	public void addToHashDictionary(String filename) {
	 	// To implement
		try {
			hashDictionary = new HashMap<>();
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = br.readLine()) != null) {
				hashDictionary.put(getPasswordHash(line), line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Do the dictionary attack.
	 */
	public void doDictionaryAttack() {
		// To implement
		addToHashDictionary(PATH + "CommonPasswords.txt");
		readPasswords(PATH + "LeakedPasswords.txt");
		passwordMap.forEach((user, password) -> {
			if (hashDictionary.containsKey(password)) {
				System.out.println("User: " + user + " Password: " + hashDictionary.get(password));
			}
		});
	}

	public static void main(String[] args) {
		DictionaryAttack da = new DictionaryAttack();
		// To implement
		da.doDictionaryAttack();
	}

}
