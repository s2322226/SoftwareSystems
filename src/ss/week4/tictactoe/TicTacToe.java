package ss.week4.tictactoe;

import ss.utils.TextIO;

public class TicTacToe {

    static Game game;

    public static void main(String[] args) {
        String prompt = "Who is playing?";
        System.out.println(prompt);
        String input = TextIO.getln();
        String[] names = input.split(" ");

        while (names.length < 2) {
            System.out.println("You need 2 players to play TicTacToe, you fool!");
            System.out.println(prompt);
            input = TextIO.getln();
            names = input.split(" ");
        }

        HumanPlayer player1 = new HumanPlayer(names[0], Mark.OO);
        HumanPlayer player2 = new HumanPlayer(names[1], Mark.XX);
        game = new Game(player1, player2);
        game.start();
    }

}
