package ss.week4;

import java.util.ArrayList;
import java.util.List;

public class MergeSort {

	public static <E extends Comparable<E>> List<E> mergeSort(List<E> data) {
		 // TODO: implement, see exercise P-4.3
		if (data.size() == 0 || data.size() == 1) {
			return data;
		} else {
			List<E> fst = mergeSort(data.subList(0, data.size() / 2));
            List<E> snd = mergeSort(data.subList(data.size() / 2, data.size()));
            List<E> result = new ArrayList<>();
            int fi = 0, si = 0;

            while (fi < fst.size() && si < snd.size()) {
				if ((int) fst.get(fi) < (int) snd.get(si)) {
					result.add(fst.get(fi));
					fi++;
				} else {
					result.add(snd.get(si));
					si++;
				}
			}

			if ( fi < fst.size()) {
				for (int i = fi; i < fst.size(); i++) {
					result.add(fst.get(i));
				}
			}
			if ( si < snd.size()) {
				for (int i = si; i < snd.size(); i++) {
					result.add(snd.get(i));
				}
			}
			return result;
		}

	}

}
