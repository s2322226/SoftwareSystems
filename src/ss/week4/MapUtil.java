package ss.week4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class MapUtil {

    /**
     * Checks if every key has a unique value.
     * @param map - input map
     * @param <K> - key
     * @param <V> - value
     * @return result
     */
    public static <K, V> boolean isOneOnOne(Map<K, V> map) {
        // TODO: implement, see exercise P-4.12
        AtomicBoolean same = new AtomicBoolean(true);
        map.forEach((k, v) -> map.forEach((k2, v2) -> {
            if (v.equals(v2) && !k.equals(k2)) {
                same.set(false);
            }
        }));
        return same.get();
    }

    /**
     * Checks if mapping is surjective
     * @param map - input map
     * @param range - range of values
     * @param <K> - key
     * @param <V> - value
     * @return result
     */
    public static <K, V> boolean isSurjectiveOnRange(Map<K, V> map, Set<V> range) {
        // TODO: implement, see exercise P-4.13
        AtomicBoolean inRange = new AtomicBoolean(true);
        range.forEach(v -> {
            if (!map.containsValue(v)) {
                inRange.set(false);
            }
        });
        return inRange.get();
    }

    /**
     * Creates a Map from original map with original values as keys, and a Set of original keys that correspond to the value.
     * @param map - input map
     * @param <K> - key
     * @param <V> - value
     * @return result
     */
    public static <K, V> Map<V, Set<K>> inverse(Map<K, V> map) {
        // TODO: implement, see exercise P-4.14
        Map<V, Set<K>> inversedMap = new HashMap<>();
        map.forEach((k, v) -> {
            if (inversedMap.containsKey(v)) {
                inversedMap.get(v).add(k);
            } else {
                Set<K> set = new HashSet<>();
                set.add(k);
                inversedMap.put(v, set);
            }
        });
        return inversedMap;
    }

    /**
     * Does the same as the inverse method, but only if the original map is injective and surjective.
     * @param map - input map
     * @param <K> - key
     * @param <V> - value
     * @return result
     */
    public static <K, V> Map<V, K> inverseBijection(Map<K, V> map) {
        // TODO: implement, see exercise P-4.14
        Map<V, K> inversedMap = new HashMap<>();
        map.forEach((k, v) -> inversedMap.put(v, k));
        return inversedMap;
    }

    /**
     * Checks whether all values in Map f are keys in Map g
     * @param f - map 1
     * @param g - map 2
     * @param <K> - key for map 1
     * @param <V> - value for map 1; key for map 2
     * @param <W> - value for map 2
     * @return result
     */
    public static <K, V, W> boolean compatible(Map<K, V> f, Map<V, W> g) {
        // TODO: implement, see exercise P-4.15
        AtomicBoolean compatible = new AtomicBoolean(true);
        f.forEach((k, v) -> {
            if (!g.containsKey(v)) {
                compatible.set(false);
            }
        });

        return compatible.get();
    }

    /**
     * Creates a new Map with key from Map 1 and corresponding value from Map g.
     * @param f - map 1
     * @param g - map 2
     * @param <K> - key for map 1
     * @param <V> - value for map 1; key for map 2
     * @param <W> - value for map 2
     * @return result
     */
    public static <K, V, W> Map<K, W> compose(Map<K, V> f, Map<V, W> g) {
        // TODO: implement, see exercise P-4.15
        Map<K, W> composed = new HashMap<>();
        if (compatible(f, g)) {
             f.forEach((k, v) -> composed.put(k, g.get(v)));
            return composed;
        }
        return null;
    }
	
}
