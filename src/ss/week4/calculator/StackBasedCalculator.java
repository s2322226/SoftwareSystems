package ss.week4.calculator;

import ss.calculator.Calculator;
import ss.calculator.DivideByZeroException;
import ss.calculator.StackEmptyException;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StackBasedCalculator implements Calculator {
    Stack<Double> stack;

    public StackBasedCalculator() {
        this.stack = new Stack<>();
    }

    @Override
    public void push(double value) {
        stack.push(value);
    }

    @Override
    public double pop() throws StackEmptyException {
        if (stack.empty()) {
            throw new StackEmptyException("Stack is empty");
        } else {
            return stack.pop();
        }
    }

    @Override
    public void add() throws StackEmptyException {
        if (stack.size() >= 2) {
            push(pop() + pop());
        } else {
            throw new StackEmptyException("Stack does not contain enough items. Needs at least 2, but has " + stack.size());
        }
    }

    @Override
    public void sub() throws StackEmptyException {
        if (stack.size() >= 2) {
            double value1 = pop();
            double value2 = pop();
            push(value2 - value1);
        } else {
            throw new StackEmptyException("Stack does not contain enough items. Needs at least 2, but has " + stack.size());
        }
    }

    @Override
    public void mult() throws StackEmptyException {
        if (stack.size() >= 2) {
            push(pop() * pop());
        } else {
            throw new StackEmptyException("Stack does not contain enough items. Needs at least 2, but has " + stack.size());
        }
    }

    @Override
    public void div() throws DivideByZeroException, StackEmptyException {
        if (stack.size() >= 2) {
            double value1 = pop();
            double value2 = pop();
            if (value1 == 0) {
                push(Double.NaN);
                throw new DivideByZeroException("Divided by zero");
            } else {
                push(value2 / value1);
            }
        } else {
            throw new StackEmptyException("Stack does not contain enough items. Needs at least 2, but has " + stack.size());
        }
    }

    @Override
    public void dup() throws StackEmptyException {
        if (stack.size() == 0) {
            throw new StackEmptyException("Stack is empty");
        } else {
            push(stack.lastElement());
        }
    }

    @Override
    public void mod() throws StackEmptyException {
        if (stack.size() >= 2) {
            double value1 = pop();
            double value2 = pop();
            push(value2 % value1);
        } else {
            throw new StackEmptyException("Stack does not contain enough items. Needs at least 2, but has " + stack.size());
        }
    }
}
