package ss.week4.calculator;

import ss.calculator.Calculator;
import ss.calculator.CalculatorFactory;
import ss.calculator.CalculatorServer;
import ss.calculator.StreamCalculator;

import java.io.Reader;
import java.io.Writer;

public class StackBasedCalculatorFactory implements CalculatorFactory {
    @Override
    public Calculator makeCalculator() {
        StackBasedCalculator calculator = new StackBasedCalculator();
        return calculator;
    }

    @Override
    public StreamCalculator makeStreamCalculator(Calculator calculator) {
        return null;
    }

    @Override
    public Runnable makeRunnableStreamCalculator(Reader reader, Writer writer) {
        return null;
    }

    @Override
    public CalculatorServer makeCalculatorServer() {
        return null;
    }
}
