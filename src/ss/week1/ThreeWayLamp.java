package ss.week1;

import ss.utils.TextIO;

public class ThreeWayLamp {

    public enum LampSetting {
        OFF,
        LOW,
        MEDIUM,
        HIGH,
        STATE,
        NEXT,
        HELP,
        EXIT
    }

    static LampSetting currentSetting = LampSetting.OFF;

    public static void main(String[] args) {
        boolean exit = false;

        while(!exit) {
            String input = TextIO.getln();

            switch (input) {
                case "OFF":
                    setLampSetting(LampSetting.OFF);
                    break;
                case "LOW":
                    setLampSetting(LampSetting.LOW);
                    break;
                case "MEDIUM":
                    setLampSetting(LampSetting.MEDIUM);
                    break;
                case "HIGH":
                    setLampSetting(LampSetting.HIGH);
                    break;
                case "STATE":
                    System.out.println(getSetting());
                    break;
                case "NEXT":
                    switchSetting();
                    break;
                case "HELP":
                    System.out.println("You can use the following options:");
                    for (LampSetting setting : LampSetting.values()) {
                        System.out.println(setting);
                    }
                    break;
                case "EXIT":
                    exit = true;
                    break;
            }
        }
    }

    public static void switchSetting() {
        switch (currentSetting) {
            case OFF:
                setLampSetting(LampSetting.LOW);
                break;
            case LOW:
                setLampSetting(LampSetting.MEDIUM);
                break;
            case MEDIUM:
                setLampSetting(LampSetting.HIGH);
                break;
            case HIGH:
                setLampSetting(LampSetting.OFF);
                break;
        }
        System.out.println(getSetting());
    }

    public static LampSetting getSetting() {
        return currentSetting;
    }

    public static void setLampSetting(LampSetting setting) {
        currentSetting = setting;
    }
}
