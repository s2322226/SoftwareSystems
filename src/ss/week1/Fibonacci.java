package ss.week1;


import ss.utils.TextIO;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

    public static void main(String[] args) {
        int value = TextIO.getInt();
        System.out.println(fibonacciRecursive(value));
        System.out.println(fibonacciWithArray(value));
    }

    public static long fibonacciRecursive(int value) {
        if (value > 1) {
            return fibonacciRecursive(value-1) + fibonacciRecursive(value-2);
        } else if (value == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public static long fibonacciWithArray(int value) {
        List<Long> sequence = new ArrayList<>();
        sequence.add(0L);
        sequence.add(1L);

        for (int i = 2; i <= value; i++) {
            sequence.add(sequence.get(i-1) + sequence.get(i-2));
        }

        return sequence.get(value);
    }
}
