package ss.week1;

import ss.utils.TextIO;

public class GrossAndDozens {

    public static void main(String[] args) {
        System.out.println("How many eggs do you have?");

        int input = TextIO.getlnInt();
        int gross = input / 144;
        int dozen = (input % 144) / 12;
        int leftover = (input % 144) % 12;

        System.out.println("Your number of eggs is " + gross + " gross, " + dozen + " dozen, and " + leftover);
    }
}
