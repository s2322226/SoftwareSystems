package ss.week1;

import java.util.ArrayList;
import java.util.List;

public class MostDivisors {

    public static void main(String[] args) {
        List<Integer> highestDivisors = new ArrayList<>();
        int highestDivisorCount = 0;

        int maxDivisor = 10000;

        for (int i = 1; i <= maxDivisor; i++) {
            int divisorCount = 0;

            for (int testDivisor = 1; testDivisor <= i; testDivisor++) {
                if (i % testDivisor == 0) divisorCount++;
            }

            if (divisorCount > highestDivisorCount) {
                highestDivisors.clear();
                highestDivisors.add(i);
                highestDivisorCount = divisorCount;
            } else if (divisorCount == highestDivisorCount) highestDivisors.add(i);
        }

        printLine("Among integers between 1 and 10000,");
        printLine("The maximum number of divisors was " + highestDivisorCount);
        printLine("Numbers with that many divisors include: ");

        for (int number : highestDivisors) {
            printLine(String.valueOf(number));
        }
    }

    public static void printLine(String line) {
        System.out.println(line);
    }
}
