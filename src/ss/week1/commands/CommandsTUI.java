package ss.week1.commands;

import ss.utils.TextIO;
import ss.week1.BabylonianAlgorithm;
import ss.week1.Fibonacci;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandsTUI {

    public static void main(String[] args) {
        printHelp();

        boolean exit = false;

        while (!exit) {
            String input = TextIO.getln();

            String[] split = input.split(" ");
            String command = split[0];
            String parm = null;

            if (split.length > 1) {
                parm = split[1];
            }

            if (parm == null && !command.equals("EXIT") && !command.equals("HELP")) {
                printLine("Make sure to insert a parameter!");
            } else {
                String result;
                switch(command) {
                    case "BABY":
                        result = String.format("%.2f", BabylonianAlgorithm.babylonianAlgorithm(Double.parseDouble(parm)));
                        printLine(result);
                        break;
                    case "FIB":
                        result = String.valueOf(Fibonacci.fibonacciRecursive(Integer.parseInt(parm)));
                        printLine(result);
                        break;
                    case "MAX":
                        split[0] = "0";
                        printLine(String.valueOf(findMaximum(split)));
                        break;
                    case "HELP":
                        printHelp();
                        break;
                    case "EXIT":
                        exit = true;
                        break;
                }
            }
        }
    }

    public static int findMaximum(String[] values) {
        List<Integer> intValues = new ArrayList<>();
        for (String value : values) {
            intValues.add(Integer.valueOf(value));
        }

        return Collections.max(intValues);
    }

    public static void printHelp() {
        printLine("Greetings Human!");
        printLine("Through this application you have access to the following commands:");
        printLine(" 1. Fibonacci number: FIB integer");
        printLine(" 2. Babylonian Algorithm: BABY double");
        printLine(" 3. Maximum number: MAX list_of_integers");
        printLine(" 4. Help menu: HELP");
        printLine(" 5. Exit application: EXIT");
    }

    public static void printLine(String line) {
        System.out.println(line);
    }
}
