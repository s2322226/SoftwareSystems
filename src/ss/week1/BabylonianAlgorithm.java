package ss.week1;

import ss.utils.TextIO;

import static java.lang.Math.abs;

public class BabylonianAlgorithm {

    public static void main(String[] args) {
        double value = TextIO.getDouble();
        System.out.println(String.format("%.2f", babylonianAlgorithm(value)));
    }

    public static double babylonianAlgorithm(double value) {
        double guess = value / 2;
        double lastGuess = 0;
        double r;

        while (abs(guess - lastGuess) / guess >= 0.01) {
            r = value / guess;
            lastGuess = guess;
            guess = (guess + r) / 2;
        }

        return guess;
    }

}
