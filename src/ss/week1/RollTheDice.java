package ss.week1;

import java.util.ArrayList;
import java.util.List;

public class RollTheDice {

    public static void main(String[] args) {
        List<String> rollStrings = new ArrayList<>();
        rollStrings.add("The first die comes up ");
        rollStrings.add("The second die comes up ");
        rollDices(rollStrings);
    }

    public static int rollDice(int sides) {
        return (int) (Math.random()*6) + 1;
    }

    public static void rollDices(List<String> rollStrings) {
        int result = 0;
        for (String rollString : rollStrings) {
            int roll = rollDice(6);
            result += roll;
            System.out.println(rollString + roll);
        }
        System.out.println("Your final rsult is " + result);
    }

}
