package ss.week5.test;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import java.nio.charset.StandardCharsets;

/**
 * A simple class that experiments with the Hex encoding
 * of the Apache Commons Codec library.
 *
 */
public class EncodingTest {
    public static void main(String[] args) throws DecoderException {
        String input = "Hello_World";
        String input2 = "Hello_Big_World";

        System.out.println(Hex.encodeHexString(input.getBytes()));
        System.out.println(Hex.encodeHexString(input2.getBytes()));

        String hex = "4d6f64756c652032";
        byte[] bytes = Hex.decodeHex(hex);
        String idk = new String(bytes);
        System.out.println(idk);

        System.out.println(Base64.encodeBase64(input.getBytes()));
        String heks = "010203040506";
        byte[] bijts = Hex.decodeHex(heks);
        System.out.println(Base64.encodeBase64(bijts));
        // its smaller, cus size matters

        String base64 = "U29mdHdhcmUgU3lzdGVtcw==";
        byte[] base64bytes = Base64.decodeBase64(base64);
        String base64String = new String(base64bytes);
        System.out.println(base64String);

        String a = "";
        for (int i = 0; i < 8; i++) {
            a += "a";
            byte[] bitties = a.getBytes();
            System.out.println(Base64.encodeBase64(bitties));
        }

    }
}
