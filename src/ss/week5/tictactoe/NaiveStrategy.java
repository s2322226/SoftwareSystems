package ss.week5.tictactoe;

import ss.week4.tictactoe.Board;
import ss.week4.tictactoe.Mark;

import java.util.ArrayList;
import java.util.List;

public class NaiveStrategy implements Strategy {

    @Override
    public String getName() {
        return "Naive";
    }

    @Override
    public int determineMove(Board board, Mark mark) {
        List<Integer> emptyFields = new ArrayList<>();

        for (int i = 0; i < board.fields.length; i++) {
            if (board.isEmptyField(i)) {
                emptyFields.add(i);
            }
        }

        int moveIndex = (int) (Math.random() * emptyFields.size());
        int move = emptyFields.get(moveIndex);
        System.out.println("Computer picks: " + move);

        return move;
    }
}
