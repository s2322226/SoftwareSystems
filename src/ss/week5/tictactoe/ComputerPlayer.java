package ss.week5.tictactoe;

import ss.week4.tictactoe.Board;
import ss.week4.tictactoe.Mark;

public class ComputerPlayer extends Player {

    Strategy strategy;

    public ComputerPlayer(Mark mark, Strategy strategy) {
        super(strategy.getName() + "-" + mark.name(), mark);
        this.strategy = strategy;
    }
    /**
     * Creates a new Player object.
     *
     * @param mark
     */
    public ComputerPlayer(Mark mark) {
        super("Naive-" + mark.name(), mark);
        this.strategy = new NaiveStrategy();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public Mark getMark() {
        return super.getMark();
    }

    @Override
    public int determineMove(Board board) {
        return strategy.determineMove(board, getMark());
    }

    @Override
    public void makeMove(Board board) {
        super.makeMove(board);
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
