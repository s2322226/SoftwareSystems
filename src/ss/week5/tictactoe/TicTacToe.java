package ss.week5.tictactoe;

import ss.utils.TextIO;
import ss.week4.tictactoe.Mark;

public class TicTacToe {

    static Game game;

    public static void main(String[] args) {
        String prompt = "Who is playing?";
        System.out.println(prompt);
        String input = TextIO.getln();
        String[] names = input.split(" ");

        while (names.length < 2) {
            System.out.println("You need 2 players to play TicTacToe, you fool!");
            System.out.println(prompt);
            input = TextIO.getln();
            names = input.split(" ");
        }

        game = new Game(determinePlayerType(names[0], Mark.XX), determinePlayerType(names[1], Mark.OO));
        game.start();
    }

    public static Player determinePlayerType(String name, Mark rutte) {
        Player player;
        switch (name) {
            case "-N":
                player = new ComputerPlayer(rutte);
                break;
            case "-S":
                player = new ComputerPlayer(rutte, new SmartStrategy());
                break;
            default:
                player = new HumanPlayer(name, rutte);
        }
        return player;
    }

}
