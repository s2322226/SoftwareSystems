package ss.week5.tictactoe;

import ss.week4.tictactoe.Board;
import ss.week4.tictactoe.Mark;

public interface Strategy {

    /**
     * Returns the name of the strategy.
     * @return result
     */
    String getName();

    /**
     * Determines next move of a player.
     * @param board
     * @param mark
     * @return next legal move
     */
    public int determineMove(Board board, Mark mark);
}
