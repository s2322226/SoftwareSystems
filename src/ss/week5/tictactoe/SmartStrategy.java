package ss.week5.tictactoe;

import ss.week4.tictactoe.Board;
import ss.week4.tictactoe.Mark;

import java.util.ArrayList;
import java.util.List;

public class SmartStrategy implements Strategy {

    @Override
    public String getName() {
        return "Smart";
    }

    @Override
    public int determineMove(Board board, Mark rutte) {
        Mark hugo = Mark.OO;

        if (rutte == Mark.OO) {
            hugo = Mark.XX;
        }

        List<Integer> emptyFields = getEmptyFields(board);

        if (board.isEmptyField(4)) {
            return 4;
        }

        int move = checkGuaranteedWin(board, emptyFields, rutte);
        if (move != -1) {
            return move;
        }

        move = checkGuaranteedWin(board, emptyFields, hugo);
        if (move != -1) {
            return move;
        }

        int moveIndex = (int) (Math.random()*emptyFields.size());
        move = emptyFields.get(moveIndex);

        return move;
    }

    public List<Integer> getEmptyFields(Board board) {
        List<Integer> emptyFields = new ArrayList<>();

        for (int i = 0; i < board.fields.length; i++) {
            if (board.isEmptyField(i)) {
                emptyFields.add(i);
            }
        }
        return emptyFields;
    }

    public int checkGuaranteedWin(Board board, List<Integer> emptyFields, Mark rutte) {
        for (int field : emptyFields) {
            Board tempBoard = board.deepCopy();
            tempBoard.setField(field, rutte);
            if (tempBoard.isWinner(rutte)) {
                return field;
            }
        }

        return -1;
    }
}
