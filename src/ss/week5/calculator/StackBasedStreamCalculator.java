package ss.week5.calculator;

import ss.calculator.Calculator;
import ss.calculator.DivideByZeroException;
import ss.calculator.StackEmptyException;
import ss.calculator.StreamCalculator;

import java.io.*;

public class StackBasedStreamCalculator implements StreamCalculator {
    Calculator calculator;

    public StackBasedStreamCalculator(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public void process(Reader input, Writer output) {
        PrintWriter pw = new PrintWriter(output, true);

        try (var br = new BufferedReader(input)) {
            br.lines().forEach(line -> {
                System.out.println(line);
                String[] args = line.split(" ");
                try {
                    switch (args[0]) {
                        case "push":
                            if (args.length >= 2) {
                                calculator.push(Double.parseDouble(args[1]));
                            } else {
                                pw.println("error: No argument given for push");
                            }
                            break;
                        case "pop":
                            pw.println(calculator.pop());
                            break;
                        case "add":
                            calculator.add();
                            break;
                        case "sub":
                            calculator.sub();
                            break;
                        case "mult":
                            calculator.mult();
                            break;
                        case "div":
                            calculator.div();
                            break;
                        case "mod":
                            calculator.mod();
                            break;
                        case "dup":
                            calculator.dup();
                            break;
                        case "value":
                            calculator.dup();
                            pw.println(calculator.pop());
                            break;
                        case "":
                            pw.println("error: Empty string found");
                            break;
                        default:
                            pw.println("error: No valid command found");
                    }
                } catch (StackEmptyException e) {
                    pw.println("error: No value found");
                } catch (DivideByZeroException e) {
                    pw.println("error: Failed to divide by zero");
                } catch (NumberFormatException e) {
                    pw.println("error: Invalid value");
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        pw.close();

    }
}
