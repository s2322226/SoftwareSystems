package ss.week5.calculator;

import java.io.*;
import java.util.Scanner;

public class InputStreamCalculator {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StackBasedCalculatorFactory factory = new StackBasedCalculatorFactory();
        var calculator = factory.makeStreamCalculator(factory.makeCalculator());
        System.out.println("Wanna do some math? -push -pop -dup -add -sub -mult -div -mod");

        while (sc.hasNextLine()) {
            try (var pr1 = new PipedReader(); var pw1 = new PipedWriter(pr1);
                 var pr2 = new PipedReader(); var pw2 = new PipedWriter(pr2);
                 var br = new BufferedReader(pr2); var pw = new PrintWriter(pw1)) {

                pw.println(sc.nextLine());

                // uses dup() and pop() to return relevant value to the system out
                pw.println("value");

                pw.close();
                calculator.process(pr1, pw2);
                System.out.println(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
