package ss.week5.calculator;

import ss.calculator.Calculator;
import ss.calculator.CalculatorFactory;
import ss.calculator.CalculatorServer;
import ss.calculator.StreamCalculator;
import ss.week4.calculator.StackBasedCalculator;
import ss.week6.RunnableCalculator;
import ss.week7.CalcServer;

import java.io.Reader;
import java.io.Writer;

public class StackBasedCalculatorFactory implements CalculatorFactory {
    @Override
    public Calculator makeCalculator() {
        StackBasedCalculator calculator = new StackBasedCalculator();
        return calculator;
    }

    @Override
    public StreamCalculator makeStreamCalculator(Calculator calculator) {
        return new StackBasedStreamCalculator(calculator);
    }

    @Override
    public Runnable makeRunnableStreamCalculator(Reader reader, Writer writer) {
        return new RunnableCalculator(reader, writer);
    }

    @Override
    public CalculatorServer makeCalculatorServer() {
        return new CalcServer();
    }
}
